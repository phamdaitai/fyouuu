import React from 'react';
import setAuthToken from './utils/setAuthToken';
import store from './redux/store';
//=================================================
import jwt_decode from 'jwt-decode';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Layout } from 'antd';
import "antd/dist/antd.css";
//=================================================
import PrivateRoute from './HOCs/PrivateRoute';
import { setCurrentUser, logoutUser } from './redux/actions/auth.action';
import Home from './components/Home';
import Login from './components/auth/Login';
import Headers from './components/Header/index';
import BreadScrumbs from './components/BreadCrumb/index';
import Siders from './components/Sider/index';
import Contents from './components/Content/index';
import Chat from './components/Chat/index';


//=================================================
//Check for token
if (localStorage.jwtToken) {
  setAuthToken(localStorage.jwtToken);
  const decoded = jwt_decode(localStorage.jwtToken);
  // Set user is authenticated
  store.dispatch(setCurrentUser(decoded));
  // Check for expried token
  const currentTime = Date.now() / 1000;
  if (decoded.exp < currentTime) {
    store.dispatch(logoutUser());
    // clear current profile
    // store.dispatch(clearCurrentProfile());
    //redirect to login
    window.location.href = '/login';
  }
}

function App({ children }) {
  return (
    <div className="App">
      <Layout>
        <Headers />
        <Layout>
          <Siders />
          <Layout style={{ padding: "0 24px 24px" }}>
            <BreadScrumbs />
            <Contents>{children}</Contents>
            <Chat />
          </Layout>
        </Layout>
      </Layout>
    </div>
    // <Router>
    //   <Route exact path="/" component={Home} />
    //   <Route exact path="/login" component={Login} />
    //   <Switch>
    //     <PrivateRoute exact path="/private" component={Home} />
    //   </Switch>
    // </Router>
  );
}

export default App;
