import React, { useState } from 'react';
import "./styles.css";
import { Button, Icon, Input } from 'antd';
import ListFriends from './ListFriends/index';

const { Search } = Input;

const Chat = () => {
  const [isToggle, setIsToggle] = useState(false);


  return (
    <div className='app-chat'>
      <Button onClick={() => setIsToggle(!isToggle)}><Icon type="wechat" />Trò chuyện</Button>
      {
        isToggle ? <ListFriends /> : <div />
      }
      <Search
        placeholder="Tìm người trò chuyện"
        onSearch={value => console.log(value)}
      />
    </div>
  )
}

export default Chat;