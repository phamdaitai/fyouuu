import React, { useState } from "react";
import { Layout, Icon, Avatar, Badge, Drawer, Button } from "antd";
import { Link } from "react-router-dom";
import "./style.css";
import { connect } from "react-redux";
import { Modal } from "antd";
import Login from '../auth/Login/index';

const { Header } = Layout;

const Headers = ({ auth }) => {
  console.log(auth);

  const [state, setState] = useState({
    visibleNotice: false,
    visibleSetting: false,
    visibleHelp: false,
    visibleUser: false,
    visibleModal: false
  });

  const showDrawerNotice = () => {
    setState({
      ...state,
      visibleNotice: true
    });
  };

  const onCloseNotice = () => {
    setState({
      ...state,
      visibleNotice: false
    });
  };
  const showDrawerSetting = () => {
    setState({
      ...state,
      visibleSetting: true
    });
  };

  const onCloseSetting = () => {
    setState({
      ...state,
      visibleSetting: false
    });
  };
  const showDrawerHelp = () => {
    setState({
      ...state,
      visibleHelp: true
    });
  };

  const onCloseHelp = () => {
    setState({
      ...state,
      visibleHelp: false
    });
  };
  const showDrawerUser = () => {
    setState({
      ...state,
      visibleUser: true
    });
  };

  const onCloseUser = () => {
    setState({
      ...state,
      visibleUser: false
    });
  };

  return (
    <Header className="header" style={{ lineHeight: "55px", height: "55px" }}>
      <div className="logo" />
      <div className="header-left">
        <div className="header-left-img">
          {/* <img
            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQMAAADCCAMAAAB6zFdcAAABDlBMVEUeICP1ODgfBAEAHyIdBAAdAAD6OTkJAAD+OTmyLzD///8AHiIWICP4ODgeGBtPJCZ6KCoAAAD1LCwOHyIRHyLyODj1MTH0Hh70JCSOFBH/+fl7Dgz1KSn1Pj75o6MoISSrLzDhNjYYBQOKAAD0GRn2SUleJiiIKizqNze6MTE6IiXeNTbWNDVpJylxKCqeLS6NAACAAABnDgz80tL5nZ33cXH929v6rq72VFT3eXn8yMj5j4/95ub2Xl74hIT7vLzHMjMzIiRHJCb3dnb6tLRYJSeVLC2FKiybZGOYPj2yjo6VTEyzmJiOHxynfHzMx8fFs7NwEA5TDAqgXl2OQD+whoaecXFyAACWV1fOycl+6El7AAAIy0lEQVR4nO2dCVfiOhuAqXFKLYk6tCxlF1AWQeWqzAI64Iyo2PEy12X8/v8f+dKibCJ0s0lJnzNn5HBOzmke37x5k6Y1EFgt4JbAmUPYJn3NTuM78B1o+A5mHaBwPBaLh/GnqPYhhfCn0VdsOAinM81E4vgkzaVOjxOJ5peviEu19K8ijDhIfRPF75lfTTGdaontL5lj8Tya+i4ea18hNhxEz8R2OpZKxdMonGgW8QcO4e8y+FMszYiDOA6AqJ4VUFrMpIZezsUz7buxgpV2gHbFH/HXxIDjIPoSGyfFCQEr7iD6DQ//VwcnYvOsqE0Gqab4KxKLMuIg3BJPR32Nt0Sx3YohPFv+EMXmaZwNB3jo/xz/vsPxVltsIzwM4ulMYjxKVtzBqdiaKIW4cPFc/KFlRpSK/ZjQs8oO8GRwHJvKfsVme/gFiopfUiw44OLfxdM40nqM/2k/i+12LKp9ikbEL2EmHKB0QmxxxdjuafRrazdWjJ6IJ/FvP9PFYvq7+BUx4YCLfm2KYiIhtmNn+k/xVzycGX46H2eK1XbAofhZK5P5mUbR3fNMprWLJwOkffMNsbNuxKEQDof1XBAd/hx/w44DA/gOfAcr64A3xwo6qFY2zFHZI33NjgOBWSDpS/bx8fHx8fHxYRAIJSkUkmRoqRiDsp3WNABBSK7ubZcOe6WDyyoMAdlMYwmA7MXedq+HW291AZC85wGX7ZeHtbIgaMs4/L9QruUOsiHJaOOLq8pOZNSa26mUqgYb04IMLnJlgZ+6uYl4getsy8uXKBBUe/u8gCZbIySgWingnfUNBPUOPy3gtStC5LALljS+7KD3Gue6IZf6YJPQVk2Y14chPMoFFlgASxt7wIIENxZ0Qu8IV3ovpiVYWdY4ckX9gAD1CL+wExpCbX5MgwNDjbNUJ0cIcoa2NBF38FYCBBsGG9cpHg8wUDO6qysczvbDVOPFeZUgUrW8PJRH/diYliB3zTSuUCpBuogszmez/ZiUIFfLphp3qJQgV00pmP5lwqwpBZRKgF2TvZga1mDfdOMKfYlRMt0L3I/X2QF0jOeCUeM3WZU0oYrZ+3waqKvXO6BkpbEwZ34libVecKimdQNWzUeBTpemihFWzQ8EHaGEUwKoWWuNajTlxZDFXnBcJAulbUsxxL0YpATrveD4XAiYnlDGBKgZDSCy/Grf74a1VKLDb9CSFsGh9V5w/OGO9TDghColgWArDDgbBrBBSiolyUYw24Yn3fshwE4w23bQoyEQzJ8EcxK0Q8P0CA4tVnnOIFxQkBWJDgVtXiE/GGCWaBhwqEPegXxAMh1okE8IhNMBHgzkEwLoEE0H2jaCmfvZH4L1JaNTDkrEb7nYWfU5Ak/+ZoO9xYITDnK+A37Dd0BDHJAtE+nIB6TnBb5HfF4IEa8Ptok7ABXSDurEayS/Vra1r+4QxFMi4W0kSjaSANl8gGg4kkJ4chTIT43YQY7sfuIl8ZRIfCMpQsFQwIFAMg5o2E4MEN5JEq4oSAe4QrgiOBgE4kXiEEjOAU/HUCC6bKJgQ3WIXCcWCBFKwgAHAql9FAr2T14hdgKBz1JQIL0gkdlUpGA7dQzoEQkEnpbTSDpymYQCmsIAZ0UiGYGibKBh/vC9bah7okfeczsQ0A41tcEroOLy8lGoU7FamgQG3J0feRr20GaRXN1LQWV6jmtP4OpooHAk6ED3jmMIdJUGY+QttwKB36dUgeWnmixAVZE8DbD0dJtpaHuibRpXykWBirPq7wKzJh/4taKAxsd8J5EvPtoBX6M6CjTAB28uorIH3hgV+tDbDShC1cOt7xH6wE0lxF3QWR/OYutZvyUKtryhAEeCsTfDmFeALqnPhyM+RgJCe5TPilMYfEfQKiv4CAmeU+B8YkTcpdcUOH3fxTOT4jROrqRRpOpFBXh2cOwIK1/OUnLOwDSgjhxZQfH7VG6gGgNsObGUFjoefKHsGFOvkXtPQYX6t0YuRs7u25RA3/uwTAOljq3MKJQ8r8D46zHnghDV26fGsV4tociW94rD+QCLhQJf7nqzMpoHuLRSKAg1T8+Js0jmXhU6VFAJrZICPEcGzM6R1B21sQ8E5uZImt6J5xwhE3MkQjS/R9oGxufIFZoTZwFXxnICv+PR3QIjgLqR1+TxtYBXdwuMYGQxLXRWbE6cZXmhQOPLox1GXvJWcWpPWzkJhDsLMqNA/u0mbgDB+yXjChaH84FS7R0JzCjAEkLzJQg9ZhRoOWGeBIaiQAMG3h7iYyQdjnn7hzeYmBSnmf3TJdQ8uewm0tQJb0TvKeyPZHKnFUU8fEfRDhPHNISLVV4pLmL0p2iEbSZHggaEw7xI5fNZbiHpT0WiMsMKXg4yCpesJoMhUhkJOQYrg0mkulBmOwoC2juq6XjDEUlgl/QV+Pj4MMTmQgKbpK/PBTY/LSSokL5AF9j8tK6zhjus/VjD/w0/fvq0lg8qrDvI5xXfgcK6Az0K2HaQzycbbDtY0xRc3xRYdhDMK42r/iPbDpSjW7Vxqx4x60CrC37fqY9KT0mSvkAXmOcgH0wmC/8OngaPR8k/pC/QBeY4wFHwN1n4rao3R3/++4/0BbrAWwe4Lmg8D64LD8+Nfz5//kz6Al1g1oGmoHBdUgd3t1hBMBgkfYEuMOtAU1BS7xrqTUFXwKCD/LpSuFev1cfeiwLWHGj5UDm6fh4M7p6P/gwVsOYgiKtDpYAFqA+vUcCeA5wLlMG9OvjbGClgzAGuC3AuKAzunycUsOQAV0Z4pXh386je304qYMlBPqhVh2q/od4XJhUw5EDbPm08DZ76/d+jGYE1B7guaPSv1P7Tw3QUMORAqw5vn9S/g1usAC8aRqyvk75AF9AcBINaddjvPah/C/8EJxVgSF+gC2AH+c9YwW2//3ibxDPC2hqDDvTt0xv1ut/HdUFwVgETDvL59WSy8ajeXf8uzFHAhANcICexhIf/FeYqYMFBQFeQVBoNZa4CFhxsKrqDpKK8TYc6/we1Kf8R2sXz8wAAAABJRU5ErkJggg=="
            alt="icon interface"
          /> */}
          <Icon type="heart" style={{ fontSize: '30px', color: 'red', lineHeight: '50px' }} />
        </div>
        <div className="header-left-text">
          <div className="header-left-text-large">
            <span>Henho</span>
            <Icon type="star" />
          </div>
          <div className="header-left-text-small">
            <span>henho</span>
          </div>
        </div>
      </div>
      <div className="header-center">
        <div className="header-center-menu">
          <div className="header-center-item">
            <Link to="">Trang chủ</Link>
          </div>
          <div className="header-center-item">
            <Link to="">Chat</Link>
          </div>
          <div className="header-center-item">
            <Link to="">Phòng hẹn hò</Link>
          </div>
        </div>
      </div>
      <div className="header-right">
        <div className="header-tool">
          <div className="header-tool-item notice">
            <Icon type="bell" onClick={showDrawerNotice} />
            <Badge count={199} overflowCount={99}>
              <Link to="" className="head-example" />
            </Badge>
            <Drawer
              title="Thông báo"
              width={300}
              onClose={onCloseNotice}
              visible={state.visibleNotice}
            >
              <div />
            </Drawer>
          </div>
          <div className="header-tool-item setting">
            <Icon type="setting" onClick={showDrawerSetting} />
            <Drawer
              title="Cài đặt"
              width={300}
              onClose={onCloseSetting}
              visible={state.visibleSetting}
            >
              <div />
            </Drawer>
          </div>
          <div className="header-tool-item help">
            <Icon
              type="question-circle"
              theme="twoTone"
              onClick={showDrawerHelp}
            />
            <Drawer
              title="Trợ giúp"
              width={300}
              onClose={onCloseHelp}
              visible={state.visibleHelp}
            >
              <div />
            </Drawer>
          </div>
          <div className="header-right-account header-tool-item">
            {
              auth.isAuthenticated ?
                (<>
                  <Avatar
                    style={{ backgroundColor: "#87d068" }}
                    icon="user"
                    onClick={showDrawerUser}
                  />
                  <span onClick={showDrawerUser}>Tên người dùng</span>
                  <Drawer
                    title="Tài khoản của bạn"
                    width={300}
                    onClose={onCloseUser}
                    visible={state.visibleUser}
                  >
                    <div className="drawer-acount">
                      <div className="drawer-account-avatar">
                        <div className="drawer-account-inner">
                          <div className="drawer-account-avatar">
                            <Avatar
                              size={80}
                              style={{ backgroundColor: "#87d068" }}
                              icon="user"
                            />
                          </div>
                          <div className="drawer-account-info">
                            <span className="drawer-account-username">
                              Người dùng
                      </span>
                            <span className="drawer-account-email">
                              nguoidung@gmail.com
                      </span>
                            <Link to="">Tài khoản</Link>
                            <Link to="">Đăng xuất</Link>
                          </div>
                        </div>
                      </div>
                    </div>
                  </Drawer>
                </>
                ) : (
                  <>
                    <Button
                      onClick={() => setState({ ...state, visibleModal: true })}
                      type="primary">Đăng nhập</Button>
                    <Modal
                      visible={state.visibleModal}
                      footer={null}
                      title="Đăng nhập"
                      onCancel={() => setState({ ...state, visibleModal: false })}
                    >
                      <Login onCancel={() => setState({ ...state, visibleModal: false })} />
                    </Modal>
                  </>
                )
            }
          </div>
        </div>
      </div>
    </Header>
  );
};

const mapStateToProps = ({ auth }) => {
  return {
    auth
  }
}

export default connect(mapStateToProps)(Headers);
