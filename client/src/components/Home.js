import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
// import PropTypes from 'prop-types';
import { logoutUser } from '../redux/actions/auth.action';

const Home = props => {
  const dispatch = useDispatch();
  const { isAuthenticated, user } = useSelector(state => state.auth);
  return (
    <div>
      <div className="title">
        <h1>Login page</h1>
        <hr />
      </div>
      <div className="main">
        {isAuthenticated ? (
          <div className="user-info">
            <div className="name">
              <label htmlFor="user-name">Full name:</label>
              <div className="user-name">{user.name}</div>
            </div>
            <div className="avatar">
              <img src={user.avatar} alt="avatar" srcSet={user.avatar} />
            </div>

            <button onClick={() => dispatch(logoutUser())}>Logout</button>
          </div>
        ) : (
          <div className="login">
            <Link to="/login">Login</Link>
          </div>
        )}
      </div>
    </div>
  );
};

Home.propTypes = {};

export default Home;
