import React from "react";
import { Layout, Menu, Icon } from "antd";
import { Link } from 'react-router-dom';
const { SubMenu } = Menu;
const { Sider } = Layout;

function Siders() {
  return (
    <Sider width={200} style={{ background: "#fff" }}>
      <Menu
        mode="inline"
        defaultSelectedKeys={["1"]}
        defaultOpenKeys={["sub1"]}
        style={{ height: "100%", borderRight: 0 }}
      >

        <Menu.Item key="1">
          <Icon type="plus" />
          Menu 1
          </Menu.Item>
        <Menu.Item key="2">
          <Link to="workreport">
            <span>
              <Icon type="laptop" />
              Menu 2
            </span>
          </Link>
        </Menu.Item>
        <Menu.Item key="3">
          <span>
            <Icon type="user" />
            Menu 3
            </span>
        </Menu.Item>
        <SubMenu
          key="sub1"
          title={
            <span>
              <Icon type="laptop" />
              Sub menu 1
            </span>
          }
        >
          <Menu.Item key="5">Menu 1</Menu.Item>
          <Menu.Item key="6">Menu 2</Menu.Item>
        </SubMenu>
      </Menu>
    </Sider>
  );
}

export default Siders;
