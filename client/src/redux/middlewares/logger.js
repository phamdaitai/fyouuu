const logger = store => next => action => {
  console.log('DISPATCHING:', action);
  let result = next(action);
  console.log('NEXT STATE:', store.getState());
  return result;
};

export default logger;
