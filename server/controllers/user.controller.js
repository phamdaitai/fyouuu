// const cloudinary = require('cloudinary');
const validateProfile = require('../validations/users/profile');
const User = require('../models/User');

module.exports = {
  getUser: (req, res) => {
    return res.json(req.user);
  },
  updateUser: async (req, res) => {
    const { errors, isValid } = validateProfile(req.body);

    if (!isValid) {
      return res.status(400).json(errors);
    }
    const oldProfile = req.user;

    if (req.body.name) oldProfile.name = req.body.name;
    if (req.body.email) oldProfile.email = req.body.email;
    if (req.body.gender) oldProfile.gender = req.body.gender;
    // if (req.file) {
    //   console.log('Have update file');
    //   try {
    //     await cloudinary.v2.uploader
    //       .upload(req.file.path, {
    //         folder: 'images/userAvatar',
    //         aspect_ratio: 1.1,
    //         gravity: 'face',
    //         crop: 'lfill',
    //       })
    //       .then(res_ => (oldProfile.image = res_.secure_url));
    //   } catch (error) {
    //     errors.FileUpload = 'Error Upload Image';

    //     return res.status(400).json(errors);
    //   }
    // }
    try {
      let user = await User.findByIdAndUpdate(
        req.user._id,
        {
          $set: oldProfile,
        },
        { new: true }
      );

      if (!user) {
        errors.NotFound = 'Not Found Error';

        return res.status(404).json(errors);
      }

      return res.json(user);
    } catch (error) {
      return res.status(404).json(error);
    }
  },
  deleteUser: async (req, res) => {
    const errors = {};

    try {
      let user = await User.findOneAndRemove({ _id: req.user.id });

      if (!user) {
        errors.NotFound = 'Not Found Error';

        return res.status(404).json(errors);
      }

      return res.json({ success: true });
    } catch (err) {
      errors.ServerError = 'Unknown server error';

      return res.status(500).json(errors);
    }
  },
};
