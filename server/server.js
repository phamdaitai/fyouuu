require('dotenv').config();
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cloudinary = require('cloudinary');
const cors = require('cors');
const createError = require('http-errors');
const bodyParser = require('body-parser');
const session = require('express-session');
const passport = require('passport');
const database = require('./configs/db');
// const cloudinary = require('cloudinary');
const formData = require('express-form-data');
const helmet = require('helmet');
const Key = require('./configs/key');
const app = express();

app.use(cors());
app.use(helmet());
const sess = session({
  secret: process.env.SESSION_KEY || 'Hello world',
  resave: true,
  saveUninitialized: true,
  cookie: {},
});
// // when setting secure: true, as compliant clients will not send the cookie back to the server in the future if the browser does not have an HTTPS connection.

app.use(formData.parse());

if (app.get('env') === 'production') {
  app.set('trust proxy', 1); // trust first proxy
  sess.cookie.secure = true; // serve secure cookies
}
app.use(sess);

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Initialize Passport and restore authentication state, if any, from the session.
require('./configs/passport')(passport);
app.use(passport.initialize());

// Config database
database
  .then(() => {
    console.log('Database connnected!');
  })
  .catch(err => console.error(err));

// Config cloudinary
cloudinary.config(Key.CLODINARY);

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

require('./routes')(app);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

const port = 5000;

app.listen(port, () => console.log(`Listen on port ${port}`));
