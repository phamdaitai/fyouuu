const mongoose = require('mongoose');

module.exports = mongoose.connect(
  // process.env.MONGOOSE_URL_ONLINE,
  process.env.MONGOOSE_URL,
  {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  }
);
