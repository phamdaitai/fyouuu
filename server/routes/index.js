const openRouter = require('express').Router();
const authRouter = require('express').Router();
// const { fileFilter, storage } = require('../configs/cloudinary');
// const multer = require('multer');
// const upload = multer({ storage, fileFilter });
const passport = require('passport');

const {
  Login,
  GoogleAuth,
  FacebookAuth,
} = require('../controllers/auth.controller');
const {
  getUser,
  updateUser,
  deleteUser,
} = require('../controllers/user.controller');
const { Homepage } = require('../controllers/index.controller');

module.exports = app => {
  authRouter.use(passport.authenticate('jwt', { session: false }));

  /* GET home page. */
  openRouter.get('/', Homepage);

  // Auth
  openRouter.post('/auth/login', Login);
  // router.post('/register', Register)
  openRouter.post('/auth/google', GoogleAuth);
  openRouter.post('/auth/facebook', FacebookAuth);

  // User
  authRouter
    .route('/user')
    .get(getUser)
    // .put(upload.single('image'), updateUser)
    .put(updateUser)
    .delete(deleteUser);

  app.use('/', openRouter);
  app.use('/', authRouter);
};
