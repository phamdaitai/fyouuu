# PTPMCN

## Clone repository

```shell
git clone https://gitlab.com/tacaocanh/fyouuu.git
cd fyouuu
```

## Usage

### Config environment with `.env` file

1. Server:

```env
# server/.env
SESSION_KEY=helloworld
JWT_SECRET=thisismysecret
MONGOOSE_URL='mongodb://localhost:27017/fyouuu'
MONGOOSE_URL_ONLINE=mongodb://fyouuu:admin98@ds241268.mlab.com:41268/fyouuu
CLOUD_NAME=dx6o8ihdt
CLOUD_KEY=232118278122899
CLOUD_SECRET=zFPy31_JAGEa9S2vHs8BbH7Bh5I
```

2. Client:

```shell
# client/.env
REACT_APP_FACEBOOK_SECRET_ID=505790763305627
REACT_APP_GOOGLE_SECRET_ID=173216410222-fsjivjgrbg8b84rrvbjut0718mkssheg.apps.googleusercontent.com
```

#### Run server on port `5000`

```shell
cd server
npm run server
```

#### Run client on port `3000`

```shell
cd client
npm run client
```

### \*Option:

#### Running `mongodb` inside a `docker` container

```shell
# pull docker image
docker pull mongo

# create docker container
sudo docker run --name mongo-docker -d -p 27017:27017 -v ~/docker/volumes/mongo mongo

# run docker container
docker run mongo-docker

# stop docker container
docker stop mongo-docker
```

After that, change file `.env` to:

```shell
...
MONGOOSE_URL=mongodb://0.0.0.0:27017/fyouuu
...
```
